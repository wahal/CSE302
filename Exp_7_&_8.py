# usr/bin/python

# Architect: Mrinal Wahal

def findClass(ip):

        initial =  int(ip.split('.')[0])

        if initial>1 and initial <= 126: return ("[+] %s is a Class A IP." % ip)
        elif initial >=128 and initial <= 191: return ("[+] %s is a Class B IP." % ip)
        elif initial >=192 and initial <= 223: return ("[+] %s is a Class C IP." % ip)
        elif initial >=224 and initial <= 239: return ("[+] %s is a Class D (Multicast) IP." % ip)
        elif initial >=240 and initial <= 255: return ("[+] %s is a Class E (Restricted) IP." % ip)        
        else: return ("[-] Error: Invalid Input.")

print (findClass(input("[*] Enter IP Address: ")))